import AppDispatcher from '../dispatchers/dispatcher';
import assign  from 'react/lib/Object.assign';
import BaseStore from './base';
import Actions from '../constants/cards';

let cards = [];

const store = assign({}, BaseStore, {

    getAllCards: () => {
        return cards;
    },

    dispatcherIndex: AppDispatcher.register(function (payload) {

        var action = payload.action;

        switch (action.actionType) {
            case Actions.ADD_CARD:
                cards.push(action.data);
                store.emitChange();
                break;
        }

        return true;
    })

});

module.exports = store;