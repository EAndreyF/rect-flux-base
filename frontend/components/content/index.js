import React from 'react';
import Cards from '../cards';
import Editor from '../editor';
import CardsStore from '../../stores/cards';

export default class Content extends React.Component {

    constructor() {
        super();
        this.state = {
            cards: []
        };
    }

    componentDidMount() {
        CardsStore.addChangeListener(this.onChange);
    }

    componentWillUnmount() {
        CardsStore.addChangeListener(this.onChange);
    }

    onChange = () => {
        this.setState({
            cards: CardsStore.getAllCards()
        });
    };

    render() {
        return (
            <div className="content">
                <Cards cards={this.state.cards} />
                <Editor />
            </div>
        );
    }
};