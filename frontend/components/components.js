import React from 'react';
import Content from './content';

export default class Components extends React.Component {
    constructor() {
        super();
    }

    componentWillMount() {
    }

    render() {
        return (
            <div>
                <Content />
            </div>
        );
    }
};
