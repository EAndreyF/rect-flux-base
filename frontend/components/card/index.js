import React from 'react';

export default class Card extends React.Component {

    render() {
        let card = this.props.card;
        return (
            <div className="card">
                {card.text}
            </div>
        );
    }
};