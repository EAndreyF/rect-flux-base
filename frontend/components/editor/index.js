import React from 'react';
import CardsActions from "../../actions/cards";

export default class Editor extends React.Component {

    constructor() {
        super();
        this.state = {value: ''};
    }

    change = (e) => {
        this.setState({value: e.target.value});
    };

    addCard = () => {
        CardsActions.addCard({text: this.state.value});
        this.setState({value: ''});
    };

    render() {
        return (
            <div className="editor">
                <textarea value={this.state.value} onChange={this.change} className="editor-textarea"/>
                <button onClick={this.addCard}>Добавить текст</button>
            </div>
        );
    }
};