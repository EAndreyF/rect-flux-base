import React from 'react';
import Card from '../card';

export default class Cards extends React.Component {

    constructor() {
        super();
    }

    render() {
        let cards = this.props.cards.map((el) => {
            return (<Card card={el} />);
        });
        return (
            <div className="cards">
                {cards}
            </div>
        );
    }
};