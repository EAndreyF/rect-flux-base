import AppDispatcher from '../dispatchers/dispatcher';
import Actions from '../constants/cards';

export default {
    addCard: function(data) {
        AppDispatcher.handleViewAction({
            actionType: Actions.ADD_CARD,
            data: data
        });
    }
};